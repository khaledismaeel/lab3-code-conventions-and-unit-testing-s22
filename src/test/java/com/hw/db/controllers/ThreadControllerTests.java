/**
 * Thanks to my colleagues for their help in my despair
 * - AbdelRahman AbouNegm
 * - Mike Kuskov
 */

package com.hw.db.controllers;

import com.hw.db.models.User;
import com.hw.db.models.Forum;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;

import com.hw.db.DAO.UserDAO;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.mockito.Mockito;
import org.mockito.MockedStatic;
import static org.mockito.Mockito.mockStatic;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.util.List;
import java.util.LinkedList;

import java.util.Collections;

import org.springframework.dao.DataAccessException;

public class ThreadControllerTests {
    final String fullname = "someone";
    final String nickname = "some";
    final String email = "someone@somewhere.somedomain";
    final String about = "I hate java";

    User user;

    final Number numberOfPosts = 1729;
    final String forumSlug = "forum_slug_1337";
    final Integer numberOfThreads = 420;
    final String forumTitle = "SQRS daily";

    Forum forum;

    final String threadMessage = "What am I doing?";
    final String threadSlug = "string_slug_1337";
    final String threadTitle = "The purpose";
    final Integer numberOfVotes = 1337;
    final Integer threadID = 13;

    Thread thread;

    final String postMessage = "How exactly is this any different from a thread?";
    final boolean isEdited = false;

    Post post;

    List<Post> postList;

    threadController controller = new threadController();

    @BeforeEach
    @DisplayName("Setup")
    void setup() {
        user = new User(nickname, email, fullname, about);

        forum = new Forum(numberOfPosts, forumSlug, numberOfThreads, forumTitle, nickname);

        thread = new Thread(nickname, new Timestamp(System.currentTimeMillis()), forumTitle, threadMessage, threadSlug,
                threadTitle, numberOfVotes);
        thread.setId(threadID);

        post = new Post(nickname, new Timestamp(System.currentTimeMillis()), forumTitle, postMessage, -1, threadID,
                isEdited);

        postList = new LinkedList<>();
        postList.add(post);

        controller = new threadController();
    }

    @Test
    @DisplayName("Controller checks threads by ID")
    void checksByID() {
        try (MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(threadID)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(Integer.toString(threadID)), thread);
            assertNull(controller.CheckIdOrSlug("234526"));
        }
    }

    @Test
    @DisplayName("Controller checks threads by slug")
    void checksBySlug() {
        try (MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(threadSlug), thread);
            assertNull(controller.CheckIdOrSlug("potato"));
        }
    }

    @Test
    @DisplayName("Controller creates post")
    void createsPost() {
        try (
                MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class);
                MockedStatic<UserDAO> userMock = mockStatic(UserDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(thread);
            userMock.when(() -> UserDAO.Info(nickname)).thenReturn(user);

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postList),
                    controller.createPost(threadSlug, postList));
            assertEquals(thread, ThreadDAO.getThreadBySlug(threadSlug));
        }
    }

    @Test
    @DisplayName("Controller gets post")
    void getsPost() {
        try (MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(thread);

            threadMock.when(() -> ThreadDAO.getPosts(threadID, 1, 100, "flat", false)).thenReturn(postList);

            threadMock.when(() -> ThreadDAO.getPosts(threadID, 0, 100, "flat", false))
                    .thenReturn(Collections.emptyList());

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postList),
                    controller.Posts(threadSlug, 1, 100, "flat", false));

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()),
                    controller.Posts(threadSlug, 0, 100, "flat", false));
        }
    }

    @Test
    @DisplayName("Controller changes thread")
    void changesThread() {
        try (MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class)) {
            final String threadSlug2 = "thread_2_slug_1337";
            final Integer threadID2 = 9973;

            final Thread thread2 = new Thread(nickname, new Timestamp(System.currentTimeMillis()), forumTitle,
                    threadMessage, threadSlug2, threadTitle, 17);
            thread2.setId(threadID2);

            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug2)).thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadById(threadID2)).thenReturn(thread2);

            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(thread);

            threadMock.when(() -> ThreadDAO.change(thread, thread2)).thenAnswer((invocationOnMock) -> {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug2)).thenReturn(thread2);
                return null;
            });

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread2),
                    controller.change(threadSlug2, thread2));
            assertEquals(thread2, controller.CheckIdOrSlug(threadSlug2));
        }
    }

    @Test
    @DisplayName("Controller gets thread info")
    void getsThreadInfo() {
        try (MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(threadSlug));

            threadMock.when(() -> ThreadDAO.getThreadBySlug("missing")).thenThrow(new DataAccessException("not found") {
            });
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null).getStatusCode(),
                    controller.info("missing").getStatusCode());
        }
    }

    @Test
    @DisplayName("Contoller creates votes ")
    void createsVotes() {
        try (
                MockedStatic<ThreadDAO> threadMock = mockStatic(ThreadDAO.class);
                MockedStatic<UserDAO> userMock = mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(nickname)).thenReturn(user);
            Vote vote = new Vote(nickname, 1);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.createVote(threadSlug, vote));

        }
    }

}
